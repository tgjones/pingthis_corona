-- Setup Lime (my fork - http://github.com/tgjonesuk/Lime2DTileEngine)
package.path = package.path ..";../Lime2DTileEngine/?.lua"
lime = require ( "lime.lime" )
if config.debug then lime.enableDebugMode() end

-- Hide status bar
display.setStatusBar( display.HiddenStatusBar )

-- Enable multitouch
system.activate( "multitouch" )

-- Setup storyboard
local storyboard = require( "storyboard" )
storyboard.isDebug = config.debug
storyboard.purgeOnSceneChange = true

-- Goto splash screen
storyboard.gotoScene( "scenes.title" )
local widget = require( "widget" )
local storyboard = require( "storyboard" )
local state = require ( "state" )

local scene = storyboard.newScene()

-- Button event handlers
local function btnPlay_onEvent( event )
    local phase = event.phase 
    if phase == "ended" then
        state.currentLevel = 1
        storyboard.gotoScene( "scenes.level", "slideLeft", 500 )
    end
end

local function btnSettings_onEvent( event )
    local phase = event.phase
    if phase == "ended" then
        storyboard.gotoScene( "scenes.settings", "slideLeft", 500 )
    end
end

function scene:createScene( event )
    local group = self.view

    btnPlay = widget.newButton
    {
        id = "btn_play",
        left = 100,
        top = 100,
        label = "Play!",
        onEvent = btnPlay_onEvent
    }

    btnSettings = widget.newButton
    {
        id = "btn_settings",
        left = 100,
        top = 200,
        label = "Settings",
        onEvent = btnSettings_onEvent
    }

    group:insert(btnPlay)
    group:insert(btnSettings) 
end

scene:addEventListener( "createScene", scene )

return scene
-- Ping This! title screen
local storyboard = require( "storyboard" )
local scene = storyboard.newScene()

local titleData = {}

function gotoNextScene( event )
	storyboard.gotoScene( "scenes.menu", "slideLeft", 500 )
end

function enableTapToPlay( event )
	if titleData.tapToPlay.flashTime == nil then
		scene.view:addEventListener( "touch", gotoNextScene )
		titleData.tapToPlay.flashTime = event.time
		titleData.tapToPlay.flashDelta = 1000
	elseif event.time > titleData.tapToPlay.flashTime 
		+ titleData.tapToPlay.flashDelta then
		titleData.tapToPlay.flashTime = event.time
		titleData.tapToPlay.isVisible = not titleData.tapToPlay.isVisible
		titleData.tapToPlay.flashDelta = titleData.tapToPlay.isVisible and 1500 or 500
	end
end

function animateLogo( event )
	if titleData.logo.y < 80 then
		titleData.logo.y = titleData.logo.y + 2.5
	else
		Runtime:removeEventListener( "enterFrame", animateLogo )
		Runtime:addEventListener( "enterFrame", enableTapToPlay )
	end
end

function scene:createScene( event )
    titleData.background = display.newImageRect( assets.titleScreen, 
    	display.contentWidth, display.contentHeight )
    self.view:insert( titleData.background )
    titleData.background:setReferencePoint ( display.TopLeftReferencePoint )
    titleData.background.x = 0
    titleData.background.y = 0          
end

function scene:willEnterScene( event )
	titleData.theme = audio.loadSound( assets.titleTheme )
	titleData.themeChannel = audio.findFreeChannel ()
	if config.mute == false then
		audio.setVolume( 1, titleData.themeChannel )
		audio.play ( titleData.theme, { channel = titleData.themeChannel, loops = -1 } ) 
	end   
end

function scene:enterScene( event )
	titleData.logo = display.newImage( assets.titleLogo, 0, 0)
	self.view:insert( titleData.logo )
	titleData.logo:setReferencePoint ( display.TopCenterReferencePoint )
	titleData.logo.x = display.contentWidth / 2
	titleData.logo.y = -100
	titleData.logo.alpha = 0.9
	Runtime:addEventListener( "enterFrame", animateLogo )

	titleData.tapToPlay = display.newImage( assets.titleTapToPlay )
	self.view:insert( titleData.tapToPlay )
	titleData.tapToPlay:setReferencePoint( display.BottomCenterReferencePoint)
	titleData.tapToPlay.x = display.contentWidth / 2
	titleData.tapToPlay.y = display.contentHeight - 20
	titleData.tapToPlay.alpha = 0.9
	titleData.tapToPlay.isVisible = false
end

function scene:exitScene( event )
	Runtime:removeEventListener( "enterFrame", flashTapToPlay )
end

-- Register scene callbacks
scene:addEventListener( "createScene", scene )
scene:addEventListener( "willEnterScene", scene )
scene:addEventListener( "enterScene", scene )
scene:addEventListener( "exitScene", scene )

return scene
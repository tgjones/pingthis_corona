-- Main game scene, this is where the magic happens!
local lime = require( "lime.lime" )
local storyboard = require( "storyboard" )
local state = require( "state" )

local scene = storyboard.newScene()

function scene:createScene( event )
        local group = self.view
  
        -- Load and display map
        assert(state.currentLevel)
        local map = lime.loadMap(assets.levels[state.currentLevel])
        local visual = lime.createVisual(map)
        group:insert(visual)   

        -- Go to bottom of map
        map:setPosition(0, map.height * map.tileheight)
end
 
-- Register scene callbacks
scene:addEventListener( "createScene", scene )
  
return scene
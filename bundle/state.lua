state = {}

function state:clear ()
    for k,v in pairs(state) do 
        state[k] = nil 
    end
end

return state
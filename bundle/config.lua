-- Custom settings
config =
{	
	-- Enable/disable debug features
	debug = true,

	-- Mute sounds for development
	mute = true,

	-- Asset folders
	imageDir = "../assets/images",
	audioDir = "../assets/audio",
	levelDir = "../assets/levels",	
}

-- Asset files
assets =
{
	-- Audio
	titleTheme  = config.audioDir .. "/title_theme.mp3",

	-- Images
	titleScreen    = config.imageDir .. "/title_screen.png",
	titleLogo      = config.imageDir .. "/title_logo.png",
	titleTapToPlay = config.imageDir .. "/title_tap_to_play.png",

	-- Levels
	levels =
	{
		config.levelDir .. "/level1.tmx"
	}
}

-- iPad
if (string.sub(system.getInfo("model"), 1, 4) == "iPad") then
	application = 
	{ 
		content = 
		{	
			width       = 360, 
			height      = 480,
			fps         = 60,
			antialias   = true,
			launchPad   = false,		        
	        scale       = "letterBox",
	        xAlign      = "center",
	        yAlign      = "center",
	        imageSuffix =
	        {
	            ["@2x"] = 1.5,
	            ["@4x"] = 3.0
	        } 
		}	
	}

-- iPhone 5/iPod Touch (5th Gen)
elseif (string.sub(system.getInfo("model"), 1, 2) == "iP" and display.pixelHeight > 960) then
	application = 
	{	
		content = 
		{ 
			width       = 320, 
			height      = 568,
			fps         = 60,
			antialias   = true,
			launchPad   = false,		        
	        scale       = "letterBox",
	        xAlign      = "center",
	        yAlign      = "center",
	        imageSuffix =
	        {
	            ["@2x"] = 1.5,
	            ["@4x"] = 3.0
	        } 
		} 
	}

-- iPhone/iPod Touch
elseif (string.sub(system.getInfo("model"), 1, 2) == "iP") then
	application = 
	{ 
		content = 
		{	
			width       = 320, 
			height      = 480,
			fps         = 60,
			antialias   = true,
			launchPad   = false,		        
	        scale       = "letterBox",
	        xAlign      = "center",
	        yAlign      = "center",
	        imageSuffix =
	        {
	            ["@2x"] = 1.5,
	            ["@4x"] = 3.0
	        } 
		} 
	}

-- 16:9 Android
elseif (display.pixelHeight / display.pixelWidth > 1.72) then
    application = 
    { 
    	content = 
    	{ 
    		width       = 320, 
    		height      = 570,
   			fps         = 60,
			antialias   = true,
			launchPad   = false,		        
	        scale       = "letterBox",
	        xAlign      = "center",
	        yAlign      = "center",
	        imageSuffix =
	        {
	            ["@2x"] = 1.5,
	            ["@4x"] = 3.0
	        }  
    	} 
    }

-- 5:3 Android
else
    application = 
    { 
    	content = 
    	{ 
    		width       = 320, 
    		height      = 512,
   			fps         = 60,
			antialias   = true,
			launchPad   = false,		        
	        scale       = "letterBox",
	        xAlign      = "center",
	        yAlign      = "center",
	        imageSuffix =
	        {
	            ["@2x"] = 1.5,
	            ["@4x"] = 3.0
	        } 
    	} 
    }

end
